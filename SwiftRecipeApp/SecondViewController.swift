//
//  SecondViewController.swift
//  SwiftRecipeApp
//
//  Created by Bryan Winmill on 9/28/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet var theTitle: UILabel!
    
    @IBOutlet var theImage: UIImageView!
    
    @IBOutlet var theIngredients: UITextView!
    
    @IBOutlet var theDirections: UITextView!
    
    var info: Dictionary<String, AnyObject>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(  _ animate: Bool) {
        super.viewWillAppear(animate)
        
        theIngredients.contentInset = UIEdgeInsetsMake(-2, -2, 0, 0)
        theDirections.contentInset = UIEdgeInsetsMake(-2, -2, 0, 0)
        
        theTitle.text = info?["title"] as? String
        
        
        let temp = info?["image"] as? String
        /*
        self.theImage.image = UIImage(contentsOfFile: temp!)
        let swiftImageView: UIImageView = UIImageView(image: UIImage(contentsOfFile: temp!))
        swiftImageView.contentMode = UIViewContentMode.scaleAspectFill
        swiftImageView.contentMode = UIViewContentMode.scaleAspectFit
        swiftImageView.translatesAutoresizingMaskIntoConstraints = false
        self.theImage.addSubview(swiftImageView)*/
        
        
        /*let temp = info?["image"] as? String
        theImage.image = UIImage(named: temp!)
        self.view.addSubview(theImage)*/
        
        
        /*let temp = info?["image"] as? String
        let imageV = UIImage(named: temp!)
        let imageView = UIImageView(image: imageV)
        self.view.addSubview(imageView)*/
        
        
        /*let temp = info?["image"] as? String
        let image = UIImage(named: temp!)
        theImage.image = image*/
        
        
        /* URLs
         chicken
         http://images.meredith.com/content/dam/bhg/Images/recipe/37/R140178.jpg.rendition.largest.jpg
         lasagna
         http://images.parents.mdpcdn.com/sites/parents.com/files/styles/width_360/public/recipe/images/R156920.jpg
         chili
         http://images.meredith.com/content/dam/bhg/Images/recipe/38/R048693.jpg.rendition.smallest.ss.jpg
         */
        
        let url = URL(string: temp!)
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            print("here")
            DispatchQueue.main.async {
                self.theImage.image = UIImage(data: data!)
                self.theImage.contentMode = UIViewContentMode.scaleAspectFill
                self.theImage.contentMode = UIViewContentMode.scaleAspectFit
            }
        }
        
        //let image = UIImage(named: temp!)
        //theImage.image = UIImage(named: temp!)
        
        
        
        theIngredients.text = info?["ingredients"] as? String
        theDirections.text = info?["directions"] as? String
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
